#+TITLE: risset.net

Welcome to my blog. I decided to start putting some of my thoughts
online, mostly things related to technology and music. Perhaps some of
them will be of some use or interest to someone. At some point I will
also be putting some links to my music projects here. 

#+BEGIN_contact
[[mailto:risset@mailbox.org][email]]
[[https://gitlab.com/risset][gitlab]]
#+END_contact


* Articles
#+BEGIN_articles
[2020-07-15 Wed] [[file:posts/on-blogging.org][On Blogging]] 

[2020-07-27 Mon] [[file:posts/current-software-setup.org][Current Software Setup]] 
#+END_articles
