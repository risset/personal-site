all: create publish 

create:
	emacs --batch --load publish.el --funcall org-publish-all

publish:
	aws s3 sync ../public/ s3://risset.net/

serve:
	@python3 -m http.server --directory=../public
