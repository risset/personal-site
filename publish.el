(require 'org)

(defvar site-path-root "~/sites/risset.net/")
(defvar site-path-src (concat site-path-root "src/"))
(defvar site-path-public (concat site-path-root "public/"))

(defvar site-preamble
  "<h1><a href='../index.html'>risset.net 歓迎</a></h1>")

(defvar site-postamble
  "<footer><p>© 2020 <b>Risset</b>.  Last updated: %C</p></footer>")

(defvar index-head
  "<link rel='icon' href='./icons/favicon.png'>
   <link rel='stylesheet' href='./css/index.css'>")

(defvar post-head
  "<link rel='icon' href='../icons/favicon.png'>
  <link rel='stylesheet' href='../css/post.css'>")

(setq org-publish-project-alist
      `(("website" :components ("index" "posts" "images" "css"))

        ("index"
	 :base-directory ,site-path-src
	 :publishing-directory ,site-path-public
	 :base-extension "org"
	 :publishing-function org-html-publish-to-html
	 :section-numbers nil
	 :with-toc nil
	 :html-head-include-default-style nil
         :html-head-include-scripts nil
	 :html-head ,index-head
	 :html-preamble ,site-preamble
	 :html-postamble nil)

	("posts"
	 :base-directory ,(concat site-path-src "posts")
	 :publishing-directory ,(concat site-path-public "posts")
	 :base-extension "org"
	 :publishing-function org-html-publish-to-html
	 :section-numbers nil
	 :with-toc t
	 :html-head-include-default-style nil
         :html-head-include-scripts nil
	 :exclude ".*drafts/.*"
	 :with-date t
	 :sitemap-sort-files t
	 :html-head ,post-head
	 :html-preamble ,site-preamble
	 :html-postamble ,site-postamble)

	("images"
	 :base-directory ,(concat site-path-src "images")
	 :publishing-directory ,(concat site-path-public "images")
	 :base-extension "jpg\\|gif\\|png"
	 :publishing-function org-publish-attachment)

	("icons"
	 :base-directory ,(concat site-path-src "icons")
	 :publishing-directory ,(concat site-path-public "icons")
	 :base-extension "png\\|ico"
	 :publishing-function org-publish-attachment)

	("css"
	 :base-directory ,(concat site-path-src "css")
	 :publishing-directory ,(concat site-path-public "css")
	 :base-extension "css"
	 :publishing-function org-publish-attachment)))
